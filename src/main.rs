//! Модуль последовательного получения простых чисел методом проверки остатков от деления.
//! Первоначальные остатки от деления вычисляются делением,
//! а все последующие инкрементом и проверкой переполнения. 
//! Функция next_state структуры состояния State позволяет распараллеливать вычисления,
//! так как проверяет произвольный диапазон чисел (заданный в состоянии State).

use std::io;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::time::{Instant};
use std::fmt;


/// Структура текущего состояния вычислений
#[derive(Debug)]
struct State {
    sn_head: Vec<i64>, // Простые числа, необходимые для заполнения sn_tail
    sn_tail: Vec<i64>, // Простые числа в диапазоне p..n
    b_idx:   usize,    // Правый индекс для проверяемых остатков
    bound:   i64,      // Следующая граница, требующая увеличения длины rests
    p: i64,            // Текущее уже проверенное на простоту но не добавленное в вектор значение
    n: i64,            // Последнее число для проверки на простоту
    is_simple: bool,   // Результат проверки p на простоту
}

impl State {
    /// Конструктор структуры состояния
    fn new(n:i64) -> State {
        State {
            sn_head: vec![2, 3],
            sn_tail: vec![],
            b_idx:   0,
            bound:   3*3,
            p: 4,
            n,
            is_simple: false,
        }
    }
    /// Определяет простое ли число.
    fn is_simple(mut self)->State {
        self.is_simple = true;
        let r_idx = self.b_idx+1;
        for i in 0..r_idx {
            if self.p % &self.sn_head[i] == 0 {
                self.is_simple = false;
                return self;
            };
        };
        self
    }

    fn next_state(mut self)->State {
        if self.is_simple {
            self.sn_head.push(self.p);
        };
        self.p+=1;
        if self.p == self.bound {
            self.b_idx+=1;
            let b_idx = self.b_idx+1;
            self.bound = self.sn_head[b_idx]*self.sn_head[b_idx];
            self.is_simple = false;
        } else {
            self = self.is_simple(); // ???
        };
        self
    }
}

/// Вариант сериализации состояния
impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let vec = &self.sn_head;

        for (count, v) in vec.iter().enumerate() {
            if count != 0 { write!(f, " ")?; }
            write!(f, "{}", v)?;
        }

        // Вернём значение `fmt::Result`
        write!(f, "")
    }
}


/// Запись строки в текстовый файл
fn save_to_file(fname: &str, s: &String) {
    // Запись в файл
    //
    let path = Path::new(fname);
    let display = path.display();

    // Откроем файл в режиме для записи. Возвращается `io::Result<File>`
    let mut file = match File::create(&path) {
        Err(why) => panic!("невозможно создать {}: {}", display, why),
        Ok(file) => file,
    };

    // Запишем строку `s` в `file`. Возвращается `io::Result<()>`
    match file.write_all(s.as_bytes()) {
        Err(why) => panic!("невозможно записать в {}: {}", display, why),
        Ok(_) => (), //println!("успешно записано в {}", display),
    }
}

/// Ищет числа-близнецы и возвращает вектор их представителей
// fn get_rtwins(p_vec: Vec<i64>) -> Vec<i64> {
//     let mut twins = vec![];
//     for i in 1..p_vec.len() {
//         if p_vec[i]-p_vec[i-1] == 2 {
//             twins.push(p_vec[i]-1);
//         }
//     }
//     twins
// }

/// Ищет числа p и q, где q>2p
fn get_nums1(p_vec: Vec<i64>) -> Vec<f64> {
    let mut twins: Vec<f64>;
    let p0 = 1000000;
    twins = vec![];
    // let mut k: f64;
    let mut k_min: f64;
    let mut k_max: f64;
    let k: f64;
    k = (p_vec[p0] as f64) / (p_vec[p0-1] as f64);
    let mut p_min: f64;
    let mut p_max: f64;
    p_min = p_vec[p0] as f64;
    p_max = p_vec[p0] as f64;
    k_min = k;
    k_max = k;
    for i in p0..p_vec.len() {
        let k2 = (p_vec[i] as f64) / (p_vec[i-1] as f64);
        if k_min>k2 {
            k_min = k2;
            p_min = p_vec[i] as f64;
        }
        if k_max<k2 {
            k_max = k2;
            p_max = p_vec[i] as f64;
        }
    }
    twins.push(k_min);
    twins.push(p_min);
    twins.push(k_max);
    twins.push(p_max);
    twins
}


fn main() {
    
    let mut n2 = String::new();

    println!("Введите правую границу вычислений.");
    io::stdin()
        .read_line(&mut n2)
        .expect("Failed to read line");
    
    let n2: i64 = n2.trim().parse().expect("Пожалуйста введите число большее 3.");
    let mut state = State::new(n2);

    print!("Работа основного алгоритма ... ");
    
    let start = Instant::now();
    while state.p <= n2 {
        state = state.next_state();
    }
    let duration = start.elapsed();
    println!("{:?}", duration);

    print!("Поиск представителей чисел-близнецов ... ");
    let start = Instant::now();
    // let twins = get_rtwins(state.sn_head);
    let nums = get_nums1(state.sn_head);
    let duration = start.elapsed();
    println!("{:?}", duration);


    print!("Перевод вектора в строку ... ");
    let start = Instant::now();
    let s = format!("{:?}", nums);
    let duration = start.elapsed();
    println!("{:?}", duration);

    print!("Сохранение в файл {} ... ", "sn.txt");
    let start = Instant::now();
    save_to_file("sn.txt", &s);
    let duration = start.elapsed();
    println!("{:?}", duration);

}

